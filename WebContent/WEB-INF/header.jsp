<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<header >
	<div >
	<c:if test='${not empty sessionScope.user}'>
		<ul>
			<li><a href='/task1/index.jsp' >Home</a></li>
			<li><a href='/task1/logout' >Logout</a></li>
			
		</ul>
	</c:if>

	<c:if test='${empty sessionScope.user}'>
		<ul>
			<li><a href='/task1/index.jsp' >Home</a></li>
			<li><a href="/task1/login.jsp">Login</a></li>
			
		</ul>
	</c:if>
</div>
</header >