<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="css/login.css" media="screen">
<title>Login page</title>
</head>
<body>
	<form method="post" action="login">
		
			<h2>Login Page</h2>
			Please provide your credential to use this page
			<div>
				<label>Login: <input class="input" title="Enter login" name="login" value="" size="30" maxlength="15"/></label>
			</div>
			
			<div>
				<label>Password:<input class="input" type="password" title="Enter password" name="password" value="" size="30" maxlength="15"/></label> 
			</div>
			<input class="button"  type="submit" value="Login"/> <input class="button" type="submit" value="Registration" name="registration" />
			<span class="error">${requestScope.error}</span>	
	</form>

</body>
</html>