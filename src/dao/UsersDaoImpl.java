package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;

import beans.User;
import service.DBService;

public class UsersDaoImpl {

	private static final String INSERT_USER_SQL = "INSERT INTO user (firstName, lastName, login, password, info) values (?, ?, ?, ?, ?)";
	private static final String SELECT_USERS_SQL = "SELECT firstName, lastName, login, password, info FROM user";
	private static final String DELETE_USERS_SQL = "DELETE FROM user";
	private static final String SELECT_USER_BY_LOGIN = "SELECT firstName, lastName, login, password, info FROM user WHERE login=?";
	private static final String UPDATE_USER_SQL = "UPDATE user SET firstName = ?, lastName = ?, password = ?, info = ? WHERE login = ?";
	
	
	public UsersDaoImpl() {
	
	}

	public void loadUsers(List<User> users) throws SQLException, NamingException {
		Connection conn = null;
		Statement st = null;
		PreparedStatement psInsertUser = null;
		try {
			conn = DBService.getConnection();
			conn.setAutoCommit(false);
			st = conn.createStatement();
			st.executeUpdate(DELETE_USERS_SQL);
			psInsertUser = conn.prepareStatement(INSERT_USER_SQL);
			for (User user : users) {
				String firstName = user.getFirstName();
				String lastName = user.getLastName();
				String login = user.getLogin();
				String password = user.getPassword();
				String info = user.getInfo();

				psInsertUser.setString(1, firstName);
				psInsertUser.setString(2, lastName);
				psInsertUser.setString(3, login);
				psInsertUser.setString(4, password);
				psInsertUser.setString(5, info);

				psInsertUser.addBatch();
			}
			
			psInsertUser.executeBatch();
			conn.commit();
				
		} catch (SQLException e) {
			if (conn != null) {
				conn.rollback();
			}
		} finally {
			DBService.closeStatements(st, psInsertUser);
			DBService.closeConnection(conn);
			
		}
	}

	public List<User> getUsers() throws SQLException, NamingException {
		Statement st = null;
		ResultSet rs = null;
		Connection conn = null;
		try {
			conn = DBService.getConnection();
			st = conn.createStatement();

			rs = st.executeQuery(SELECT_USERS_SQL);
			List<User> users = new ArrayList<User>();
			while (rs.next()) {
				String firstName = rs.getString(1);
				String lastName = rs.getString(2);
				String login = rs.getString(3);
				String password = rs.getString(4);
				String info = rs.getString(5);
				users.add(new User(firstName, lastName, login, password, info));
			}
			return users;
		} finally {
			DBService.closeResultSet(rs);
			DBService.closeStatements(st);
			DBService.closeConnection(conn);
		}
	}
	
	public User getUserByLogin(String login) throws SQLException, NamingException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = DBService.getConnection();
			
			ps = conn.prepareStatement(SELECT_USER_BY_LOGIN);
			ps.setString(1, login);
			rs = ps.executeQuery();
			if(rs.next()) {
				final String firstName = rs.getString(1);
				final String lastName = rs.getString(2);
				final String pass = rs.getString(4);
				final String info = rs.getString(5);
				return new User(firstName, lastName, login, pass, info);
			}
			return null;
		} finally {
		
			DBService.closeResultSet(rs);
			DBService.closeStatements(ps);
			DBService.closeConnection(conn);
		}

	}
	
	public boolean isPersist(String login) throws SQLException, NamingException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = DBService.getConnection();
			ps = conn.prepareStatement(SELECT_USER_BY_LOGIN);
			ps.setString(1, login);
			rs = ps.executeQuery();
			if(rs.next()) {
				return true;
			}
			return false;
		} finally {
			DBService.closeResultSet(rs);
			DBService.closeStatements(ps);
			DBService.closeConnection(conn);
		}
		
	}
	
	public void update(User user) throws SQLException, NamingException {
		PreparedStatement ps = null;
		Connection conn = null;
		try {
			conn = DBService.getConnection();
			conn.setAutoCommit(false);
			ps = conn.prepareStatement(UPDATE_USER_SQL);
			ps.setString(1, user.getFirstName());
			ps.setString(2, user.getLastName());
			ps.setString(3, user.getPassword());
			ps.setString(4, user.getInfo());
			ps.setString(5, user.getLogin());
			ps.executeUpdate();	
		} catch (SQLException e) {
			if (conn != null) {
				conn.rollback();
			}
		} finally {
			DBService.closeStatements(ps);
			DBService.closeConnection(conn);
		}
	}

	

}
