package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;

import javax.naming.NamingException;

import service.DBService;

public class LoginDaoImpl {
	
	private static final String INSERT_LOGIN_SQL = "INSERT INTO authorizationlog (date, time, login, isLogin) values (?, ?, ?, ?)";
	

	
	public void insert(String login, Calendar calendar, boolean isLogin) throws NamingException, SQLException {
		PreparedStatement ps = null;
		Connection conn = null;
		try {
			conn = DBService.getConnection();
			conn.setAutoCommit(false);
			ps = conn.prepareStatement(INSERT_LOGIN_SQL);
			long time = calendar.getTimeInMillis();
			System.out.println(time);
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND, 0);
			calendar.set(Calendar.MILLISECOND, 0);
			Date date = calendar.getTime();
			time = time - calendar.getTimeInMillis();
			System.out.println(calendar.getTimeInMillis());
			ps.setDate(1, new java.sql.Date(date.getTime()) );
			ps.setLong(2, time);
			ps.setString(3, login);
			ps.setBoolean(4, isLogin);
			ps.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			if (conn != null) {
				conn.rollback();
			}
		} finally {
			DBService.closeStatements(ps);
			DBService.closeConnection(conn);
		}
	}
}
