package listeners;

import java.util.Enumeration;
import java.util.logging.Logger;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;



public class SessionListener implements HttpSessionListener {

	private static final Logger log = Logger.getLogger(SessionListener.class.getName());
	

    public void sessionCreated(HttpSessionEvent se)  { 
    	  log.info("<--------------Session created--------------->");
    	  logSession(se);
    }


    public void sessionDestroyed(HttpSessionEvent se)  { 
    	log.info("<--------------Session destroyed--------------->");
    	logSession(se);
    }
    
    private void logSession(HttpSessionEvent se) {
    	  HttpSession session = se.getSession();
          Enumeration<String> attrs = session.getAttributeNames();
        
          while(attrs.hasMoreElements()) {
         	final String attributeName = attrs.nextElement();
         	log.info(String.format("%s - %s", attributeName, session.getAttribute(attributeName)));
          }
    }
	
}
