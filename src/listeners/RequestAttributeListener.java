package listeners;

import java.util.logging.Logger;

import javax.servlet.ServletRequestAttributeEvent;
import javax.servlet.ServletRequestAttributeListener;


public class RequestAttributeListener implements ServletRequestAttributeListener {
	
	private static Logger log = Logger.getLogger(RequestAttributeListener.class.getName());
	private static final String REPLASE_LOG = "%s: old value - %s: new value %s";
	private static final String ADD_LOG = "Login: %s - %s";
	private static final String DELETE_LOG = "Logout: %s - %s";

    
	
    public void attributeRemoved(ServletRequestAttributeEvent srae)  { 
    	log.info(String.format(DELETE_LOG, srae.getName(), srae.getValue()));
    }


    public void attributeAdded(ServletRequestAttributeEvent srae)  { 
    	log.info(String.format(ADD_LOG, srae.getName(), srae.getValue()));
    }

	
    public void attributeReplaced(ServletRequestAttributeEvent srae)  { 
         log.info(String.format(REPLASE_LOG,srae.getName(),srae.getValue(),srae.getServletRequest().getAttribute(srae.getName())));
    } 
	
}
