package listeners;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import beans.User;
import dao.UsersDaoImpl;
import service.UsersGenarator;


public class PredefinedUsersLoader implements ServletContextListener {



	public void contextInitialized(ServletContextEvent sce) {
	
		ServletContext context = sce.getServletContext();
		int usersNumber = Integer.parseInt(context.getInitParameter("UsersNumber"));
		List<User> users = new ArrayList<User>();
		try {
			for (int i = 0; i < usersNumber; i++) {
			User user = UsersGenarator.generateUser();
			users.add(user);
			}

			UsersDaoImpl dao = new UsersDaoImpl();
			dao.loadUsers(users);

		} catch (SQLException | NamingException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
	
		
	}



}
