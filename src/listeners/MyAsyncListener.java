package listeners;

import java.io.IOException;

import javax.servlet.AsyncEvent;
import javax.servlet.AsyncListener;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MyAsyncListener implements AsyncListener {

	@Override
	public void onComplete(AsyncEvent event) throws IOException {
		HttpServletRequest request = (HttpServletRequest) event.getSuppliedRequest();
		HttpServletResponse response = (HttpServletResponse) event.getSuppliedResponse();
		try {
			request.getRequestDispatcher("/protected/async.jsp").forward(request, response);
		} catch (ServletException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	@Override
	public void onTimeout(AsyncEvent event) throws IOException {
		

	}

	@Override
	public void onError(AsyncEvent event) throws IOException {
		

	}

	@Override
	public void onStartAsync(AsyncEvent event) throws IOException {
		// TODO Auto-generated method stub

	}

}
