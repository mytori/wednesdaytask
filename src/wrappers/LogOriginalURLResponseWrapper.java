package wrappers;

import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

public class LogOriginalURLResponseWrapper extends HttpServletResponseWrapper {
	private static Logger log = Logger.getLogger(LogOriginalURLResponseWrapper.class.getName());
	private final HttpServletResponse original;
	public LogOriginalURLResponseWrapper(HttpServletResponse response) {
		super(response);
		original = response;
	}
	
	@Override
	public void sendRedirect(String location) throws IOException {
		log.info("Original URL: " + original.getHeader("Location"));
		super.sendRedirect(location);
	}
	
	

}
