package wrappers;

import java.util.logging.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;



public class LogOriginalURLRequestWrapper extends HttpServletRequestWrapper {

	private static Logger log = Logger.getLogger(LogOriginalURLRequestWrapper.class.getName());
	private final HttpServletRequest original; 
	
	public LogOriginalURLRequestWrapper(HttpServletRequest request) {
		
		super(request);
		original = request;
	}
	
	
	 @Override
	 public RequestDispatcher getRequestDispatcher(String path) {
		 log.info("Original URL:" + original.getRequestURL());
		 return super.getRequestDispatcher(path);
	 }
	 
	 public String getOriginalURL() {
		 return original.getRequestURL().toString();
	 }
}
