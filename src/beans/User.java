package beans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.NamingException;
import javax.servlet.http.HttpSessionActivationListener;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;
import javax.servlet.http.HttpSessionEvent;

import dao.LoginDaoImpl;

public class User implements HttpSessionBindingListener, HttpSessionActivationListener, Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6904991735545441746L;

	private static final Logger log = Logger.getLogger(User.class.getName());
	
	private String firstName;
	private String lastName;
	private String login;
	private String password;
	private String info;
	
	
	public User(String firstName, String lastName, String login, String password, String info) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.login = login;
		this.password = password;
		this.info = info;
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	


	@Override
	public void valueBound(HttpSessionBindingEvent event) {
		boundUnbound(event, true);
		
	}

	@Override
	public void valueUnbound(HttpSessionBindingEvent event) {
		boundUnbound(event, false);
		
	}
	
	private void boundUnbound(HttpSessionBindingEvent event, boolean isloged) {
		
		try {
			LoginDaoImpl dao = new LoginDaoImpl();
		
			dao.insert(this.getLogin(), new GregorianCalendar(), isloged);
		} catch (NamingException | SQLException e) {
			log.log(Level.WARNING, "Some error stack trace:", e);
		} 
	}

	@Override
	public void sessionWillPassivate(HttpSessionEvent se) {
		logSession("Session activate", se);
		
	}

	@Override
	public void sessionDidActivate(HttpSessionEvent se) {
		logSession("Session passivate", se);
		
	}
	
	private static void logSession(String msg, HttpSessionEvent se) {
    	log.info(msg);
        log.info(se.getSession().getId());
        User user = (User) se.getSession().getAttribute("User");
        if(user != null) {
        	  log.info("user - " + user.getLogin());
        }
    }
	
	
}
