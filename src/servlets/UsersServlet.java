package servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.List;
import java.util.logging.Logger;

import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.UnavailableException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;
import dao.UsersDaoImpl;
import service.Constants;


public class UsersServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String LOG_ATR = "%s:%s";
	private static Logger log = Logger.getLogger(UsersServlet.class.getName());

	private ServletContext context;
	private boolean isUnavailable;



	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		if (isUnavailable) {
			int time = Integer.parseInt(context.getInitParameter("unavailableTime"));
			isUnavailable = false;
			UnavailableException e = new UnavailableException(String.format("Page is unavailable for %d sec.", time), time);
			request.setAttribute("exception", e);
			throw e;

		} 
		List<User> users = null;
		try {
			
			UsersDaoImpl dao = new UsersDaoImpl();
			users = dao.getUsers();
			RequestDispatcher dispatcher = context.getRequestDispatcher("/protected/userList.jsp");
			request.setAttribute(Constants.USER, users);
			dispatcher.forward(request, response);	
			
		} catch (SQLException | NamingException e) {
			e.printStackTrace();
			throw new ServletException(e);
		}


	}



	@Override
	public void init(ServletConfig config) throws ServletException {
		context = config.getServletContext();
		final Enumeration<String> attributeNames = context.getAttributeNames();
		final Enumeration<String> initParamsNames = context.getInitParameterNames();
		   
	    log.info("---------------Attributes-----------------");
	    while(attributeNames.hasMoreElements()) {
	       	String name = attributeNames.nextElement();
	     	log.info(String.format(LOG_ATR, name, context.getAttribute(name)));
	    }
	    log.info("---------------Init params----------------");
	    while(initParamsNames.hasMoreElements()) {
	    	String name = initParamsNames.nextElement();
	     	log.info(String.format(LOG_ATR, name, context.getInitParameter(name)));
	    }
		
		isUnavailable = new Boolean(context.getInitParameter("isUnavailable"));
		super.init(config);			

	}
}
