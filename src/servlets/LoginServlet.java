package servlets;

import java.io.IOException;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import dao.UsersDaoImpl;
import service.Constants;


public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private static final String ERR_ATTR = "error"; 

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		final String login = request.getParameter(Constants.LOGIN);
		final String pass = request.getParameter(Constants.PASSWORD);
		try {
			UsersDaoImpl dao = new UsersDaoImpl();
			User user = dao.getUserByLogin(login);
			if(user == null) {
				showError("User not found", request, response);
			} else if(!user.getPassword().equals(pass)) {
				showError("Wrong password", request, response);
			} else {
				HttpSession session = request.getSession();
				session.setAttribute(Constants.USER, user);
		
				response.sendRedirect("index.jsp");
			}
		} catch(SQLException | NamingException e) {
			throw new ServletException(e);
		}
		
		
	}

	private void showError(String error, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute(ERR_ATTR, error);
		request.getRequestDispatcher("/login.jsp").forward(request, response);
	}
	
}
