package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class SecondAsyncServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		long start = System.currentTimeMillis() ;					
			try {
				Thread.sleep(2000);
				long duration = System.currentTimeMillis() - start;
				request.setAttribute("second", duration);	
				System.out.println(duration);
			} catch (InterruptedException e) {	
				throw new RuntimeException(e.getMessage(), e);
			} 
	}
}
