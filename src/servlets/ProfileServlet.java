package servlets;

import java.io.IOException;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;
import dao.UsersDaoImpl;
import service.Constants;


public class ProfileServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		final String firstName = request.getParameter(Constants.FIRST_NAME);
		final String lastName = request.getParameter(Constants.LAST_NAME);
		final String password = request.getParameter(Constants.PASSWORD);
		final String info = request.getParameter(Constants.INFO);
		User user = (User) request.getSession().getAttribute(Constants.USER);
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setPassword(password);
		user.setInfo(info);
		UsersDaoImpl dao;
		try {

			dao = new UsersDaoImpl();
			
			if(dao.isPersist(user.getLogin())) {
				dao.update(user);
			}
			response.sendRedirect("protected/profile.jsp");
		} catch (NamingException | SQLException e) {
			e.printStackTrace();
			throw new ServletException(e.getMessage(), e);
		} 		
	
	}
	


}
