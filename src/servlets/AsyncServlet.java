package servlets;

import java.io.IOException;

import javax.servlet.AsyncContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import listeners.MyAsyncListener;


public class AsyncServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		long start = System.currentTimeMillis();
	    final AsyncContext asyncContext = request.startAsync();
	    asyncContext.addListener(new MyAsyncListener());
	    asyncContext.setTimeout(9000);
		asyncContext.start(new Runnable() {
			
			@Override
			public void run() {
				
				try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                }
				long duration = System.currentTimeMillis() - start;
				request.setAttribute("first", duration);
				System.out.println(duration);	
				asyncContext.dispatch("/secondAsync");	
			}
		});
			
		
			
	}

}
