package service;

import java.util.Random;

import beans.User;

public class UsersGenarator {

	private static String LOGIN_DICT = "ABCDEFGHIKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890-_.";
	private static String NAME_DICT = "abcdefghijklmnopqrstuvwxyz";
	private static int MAX_STRING_LENGTH = 15;
	private static int MIN_STRING_LENGTH = 3;

	public static User generateUser() {
		String firstName = generateString(NAME_DICT);
		String lastName = generateString(NAME_DICT);
		String login = generateString(LOGIN_DICT);
		String password = generateString(LOGIN_DICT);
		String info = generateString(LOGIN_DICT);
		User user = new User(firstName, lastName, login, password, info);
		return user;
	}

	private static String generateString(String dict) {
		Random r = new Random();
		char[] cd = dict.toCharArray();
		int strLength = r.nextInt(MAX_STRING_LENGTH - MIN_STRING_LENGTH) + MIN_STRING_LENGTH;
		char[] result = new char[strLength];
		for (int i = 0; i < strLength; i++) {
			int number = r.nextInt(dict.length() - 1);
			result[i] = cd[number];
		}

		return new String(result);
	}

}
