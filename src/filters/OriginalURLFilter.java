package filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import wrappers.LogOriginalURLRequestWrapper;
import wrappers.LogOriginalURLResponseWrapper;


public class OriginalURLFilter extends AbstractFilter implements Filter {
	
	
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		LogOriginalURLRequestWrapper req = new LogOriginalURLRequestWrapper((HttpServletRequest)request);
		String location = req.getOriginalURL();
		((HttpServletResponse) response).setHeader("Location", location);
		
		chain.doFilter(req, new LogOriginalURLResponseWrapper((HttpServletResponse) response));
	}


}
